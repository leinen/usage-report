default: load-month.html load-day.html load-week.html load-year.html load.html

load-day.html: load-template.html
	sed -e s/@@INTERVAL@@/24hours/g load-template.html > load-day.html || rm load-day.html

load-week.html: load-template.html
	sed -e s/@@INTERVAL@@/172hours/g load-template.html > load-week.html || rm load-week.html

load-month.html: load-template.html
	sed -e s/@@INTERVAL@@/32days/g load-template.html > load-month.html || rm load-month.html

load-year.html: load-template.html
	sed -e s/@@INTERVAL@@/367days/g load-template.html > load-year.html || rm load-year.html

load.html: load-month.html
	rm -f load.html && ln -s load-month.html load.html
