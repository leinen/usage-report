usage-report
============

This script generates simple usage reports for OpenStack clusters.

Note that it needs a fairly recent version of python-novaclient.  If
you get an error message like

```ImportError: No module named v2```

this may mean that you need to use a newer version.

On an Ubuntu GNU/Linux 14.04 system, you should be able to install the
required software using the following commands:

```
sudo apt-get install pip python-dev
sudo pip install python-novaclient python-cinderclient oslo.config netifaces
```
