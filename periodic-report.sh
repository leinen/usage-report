#!/bin/sh

DIR=/var/www/html/reports

. $HOME/.openrc-leinen
timestamp="`date +%Y%m%d-%H%M`"

time $HOME/usage-report/usage-report.py > $DIR/report-${timestamp}.txt
$HOME/usage-report/generate-summary.pl
