#!/usr/bin/env python
#
# Copyright (c) 2015  SWITCH
#
# Licensed under the GNU Affero General Public License

"""Extract utilization information from OpenStack"""

import os
import re
import sys
from warnings import warn
from keystoneclient.v2_0 import client as keystone_client
from keystoneclient.auth.identity import v2
from keystoneclient import session
from novaclient.v2 import client as nova_client
from novaclient.exceptions import BadRequest, NotFound
from cinderclient.v2 import client as cinder_client

class UsageAnalyzer:
    """Analyze the current resource utilization of an OpenStack cluster.

    We would like to know which tenants/organizations use (or have
    reserved) how many resources of the various types in the different
    regions of our OpenStack cloud.

    In order to map tenants to organizations, or "cost centers", we
    put the name of the cost center into tenants' description strings
    surrounded by brackets, e.g.:

       $ keystone tenant-get simon.leinen@switch.ch
       +-------------+------------------------------------------------+
       |   Property  |                     Value                      |
       +-------------+------------------------------------------------+
       | description | Project for simon.leinen@switch.ch [switch.ch] |
       |   enabled   |                      True                      |
       |      id     |        4aecd5c1599f4862960dbe808e56f403        |
       |     name    |             simon.leinen@switch.ch             |
       +-------------+------------------------------------------------+

    The script builds a map from tenants to cost centers.

    The script lists all VMs and volumes in the system, computes
    resource utilization (using flavor descriptions), adds up
    utilization per tenant, and provides reports.

    """

    def __init__(self,
                 fine_grained=True,
                 username=    os.environ['OS_USERNAME'],
                 password=    os.environ['OS_PASSWORD'],
                 tenant_name= os.environ['OS_TENANT_NAME'],
                 auth_url=    os.environ['OS_AUTH_URL'],
                 ram_cut =    12 * 1024):

        self.fine_grained = fine_grained

        self.username = username
        self.password = password
        self.tenant_name = tenant_name
        self.auth_url = auth_url

        ## All cost centers that total less than this
        ## are considered "small" and thrown into one bin.
        ##
        self.ram_cut = ram_cut

        auth = v2.Password(auth_url=self.auth_url,
                           username=self.username,
                           password=self.password,
                           tenant_name=self.tenant_name)
        self.auth_session = session.Session(auth=auth)
        self.keystone = keystone_client.Client(session=self.auth_session)

        self.usage_per_tenant = dict()
        self.tenants = None
        self.cost_centers = None
        self.regions = None
        self._nova_endpoints = None
        self._cinder_endpoints = None

        self.cc_re = re.compile(".* \[(.*)\]")

    def get_regions(self):
        if self.regions is None:
            re = dict()
            endpoints = self.keystone.endpoints.list()
            for endpoint in endpoints:
                re[endpoint.region] = 1
            self.regions = re.keys()
        return self.regions

    def nova_endpoints(self):
        if self._nova_endpoints is not None:
            return self._nova_endpoints
        ne = dict()
        for region in self.get_regions():
            try:
                ne[region] = nova_client.Client(session=self.auth_session,
                                                region_name=region)

                ##
                ## In order to check whether this works, we need to
                ## actually try to run an arbitrary operation using
                ## the endpoint.  If our token is too large, we'll
                ## receive an error that we'll catch below.
                ##
                ne[region].flavors.list()
            except BadRequest as c:
                warn("Error creating nova client using session:\n  "
                     + str(c)
                     + "\nRetrying using traditional credentials")
                ne[region] = nova_client.Client(
                                    self.username, self.password,
                                    self.tenant_name, self.auth_url,
                                    region_name=region)
        self._nova_endpoints = ne
        return ne

    def cinder_endpoints(self):
        if self._cinder_endpoints is not None:
            return self._cinder_endpoints
        ne = dict()
        for region in self.get_regions():
            ne[region] = cinder_client.Client(session=self.auth_session,
                                              region_name=region)
        self._cinder_endpoints = ne
        return ne

    def cc_name_from_tenant_1(self, tenant, cc_name, fine_grained=True):
        if cc_name:
            if cc_name == 'switch.ch':
                if tenant.name.startswith('SWITCHdrive'):
                    return 'drive.switch.ch'
                if tenant.name.startswith('Extended Security Monitoring') \
                   or tenant.name.startswith('daniel.roethlisberger@') \
                   or tenant.name.startswith('daniel.stirnimann@') \
                   or tenant.name.startswith('frank.herberg@') \
                   or tenant.name.startswith('mathias.karlsson@') \
                   or tenant.name.startswith('matthias.seitz@') \
                   or tenant.name.startswith('michael.hausding@') \
                   or tenant.name.startswith('rolf.sommerhalder@') \
                   or tenant.name.startswith('serge.droz@') \
                   or tenant.name.startswith('sybil.ehrensberger@') \
                   or tenant.name.startswith('Distributed Splunk') \
                   or tenant.name.startswith('Gremlins'):
                    return 'cert.switch.ch'
                if not fine_grained:
                    return 'switch.ch'
                if tenant.name.startswith('[cast-ng]'):
                    return 'InEn.switch.ch'
                if tenant.name.startswith('SWITCHcollection'):
                    return 'InEn.switch.ch'
                if tenant.name.startswith('SWITCHportfolio'):
                    return 'InEn.switch.ch'
                if tenant.name.startswith('SWITCHtube'):
                    return 'InEn.switch.ch'
            if fine_grained:
                return cc_name
            else:
                return 'other'
        elif tenant.name == 'admin' or tenant.name == 'peta' \
             or tenant.name == 'test' or tenant.name == 'testtenant':
            return 'switch.ch'
        else:
            if fine_grained:
                return tenant.name
            else:
                return 'other'

    def cost_center_name_from_tenant(self, tenant, fine_grained=None):
        cc_name = None
        if fine_grained is None:
            fine_grained = self.fine_grained
        if tenant.description is not None:
            m = self.cc_re.match(tenant.description)
            if m:
                cc_name = m.group(1)
        return self.cc_name_from_tenant_1(tenant, cc_name, fine_grained=fine_grained)

    def get_cost_centers(self, fine_grained=None):
        if fine_grained is None:
            fine_grained = self.fine_grained
        if self.cost_centers is not None:
            return self.cost_centers
        ccs = dict()
        tenants = self.keystone.tenants.list()
        for tenant in tenants:
            cc = self.cost_center_name_from_tenant(tenant, fine_grained=fine_grained)
            ccs[cc] = ccs.get(cc, CostCenter(cc))
            ccs[cc].tenants.append(tenant)
        self.cost_centers = ccs
        return ccs

    def retrieve_flavor(self, region, id):
        ne = self.nova_endpoints()[region]
        f = ne.flavors.get(id)
        self.flavors[region][f.id] = \
            Flavor(id=f.id, name=f.name, region=region,
                   ram=f.ram, vcpus=f.vcpus, disk=f.disk)
        return self.flavors[region][f.id]

    def get_flavor(self, id, region):
        if self.flavors is None:
            self.collect_flavors()
        return self.flavors.get(region, {}).get(id, self.retrieve_flavor(region, id))

    def note_usage(self, tenant_id, region, status,
                   instances=1, ram=0, vcpus=0, disk=0):
        ut = self.usage_per_tenant.get(tenant_id, UsagePerTenant(tenant_id))
        self.usage_per_tenant[tenant_id] = ut
        utr = ut.per_region.get(region, UsagePerTenantRegion(tenant_id, region))
        ut.per_region[region] = utr
        utrs = utr.per_status.get(status, UsagePerTenantRegionStatus(tenant_id, region, status))
        utr.per_status[status] = utrs
        ru = utrs.usage
        ru.instances += instances
        ru.ram       += ram
        ru.vcpus     += vcpus
        ru.disk      += disk

    def collect_servers(self):
        nes = self.nova_endpoints()
        for r in self.get_regions():
            ne = nes[r]
            servers = ne.servers.list(search_opts = {'all_tenants': 1})
            for server in servers:
                try:
                    s = ne.servers.get(server.id)
                    f = self.get_flavor(s.flavor['id'], r)
                    self.note_usage(s.tenant_id, r,
                                    s.status,
                                    ram=f.ram, vcpus=f.vcpus, disk=f.disk)
                except NotFound as c:
                    warn("Instance {:s} disappeared".format(server.id))

    def collect_volumes(self):
        ces = self.cinder_endpoints()
        for r in self.get_regions():
            ce = ces[r]
            volumes = ce.volumes.list(search_opts = {'all_tenants': 1})
            for volume in volumes:
                try:
                    v = ce.volumes.get(volume.id)
                    self.note_usage(v._info['os-vol-tenant-attr:tenant_id'], r,
                                    v.status,
                                    instances=0, ram=0, vcpus=0, disk=v.size)
                except NotFound as c:
                    warn("Volume {:s} disappeared".format(volume.id))

    def collect_flavors(self):
        self.flavors = dict()
        nes = self.nova_endpoints()
        for r in self.get_regions():
            self.flavors[r] = dict()
            ne = nes[r]
            flavors = ne.flavors.list()
            for flavor in flavors:
                self.retrieve_flavor(r, flavor.id)

    def status_values(self):

        """
        Compute the set of all "status" values for which we have usage records
        """

        statuses=set()
        for tenant_id, usage in self.usage_per_tenant.iteritems():
            t_usage = self.usage_per_tenant.get(tenant_id, None)
            if t_usage is None:
                continue
            for region in self.get_regions():
                r_usage = t_usage.per_region.get(region, None)
                if r_usage is None:
                    continue
                for status, usage in r_usage.per_status.iteritems():
                    statuses.add(status)
        return statuses

    def tenant_report(self, per_region=True, active_only=False, any_status=False):

        def status_set(active_only, any_status):
            if active_only:
                return ['ACTIVE', 'in-use']
            if any_status:
                return self.status_values()
            return ['ACTIVE', 'in-use', 'available', 'error', 'PAUSED', 'SUSPENDED', 'ERROR']

        def title():
            return "Status " + ", ".join(sorted(status_set(active_only, any_status)))

        def out_title():
            print(title() + "\n")

        def insignificant_usage(usage):
            return usage['total'].ram < self.ram_cut

        small_usage = dict()
        small_usage['per_region'] = dict()
        small_usage['total'] = ResourceUsage()
        for region in self.get_regions():
            small_usage['per_region'][region] = ResourceUsage()

        total_usage = dict()
        total_usage['per_region'] = dict()
        total_usage['total'] = ResourceUsage()
        for region in self.get_regions():
            total_usage['per_region'][region] = ResourceUsage()

        usage_per_cc = dict()
        interesting_statuses = status_set(active_only, any_status)
        for cc_name, cc in self.get_cost_centers().iteritems():
            usage_per_cc[cc_name] = dict()
            usage_per_cc[cc_name]['total'] = ResourceUsage()
            usage_per_cc[cc_name]['per_region'] = dict()
            for region in self.get_regions():
                usage_per_cc[cc_name]['per_region'][region] = ResourceUsage()
            for tenant in cc.tenants:
                t_usage = self.usage_per_tenant.get(tenant.id, None)
                if t_usage is None:
                    continue
                for region in self.get_regions():
                    r_usage = t_usage.per_region.get(region, None)
                    if r_usage is None:
                        continue
                    for status in interesting_statuses:
                        usage = r_usage.per_status.get(status, None)
                        if usage:
                            usage = usage.usage
                            usage_per_cc[cc_name]['per_region'][region].add(usage)
                            usage_per_cc[cc_name]['total'].add(usage)
                            total_usage['per_region'][region].add(usage)
                            total_usage['total'].add(usage)
                            #print tenant.name, region, usage.instances, usage.vcpus, usage.ram
        def cc_weight(cc_name):
            usage_per_cc[cc_name]['total'].ram

        def out_cc(cc_name, usage):
            if per_region:
                sys.stdout.write("{:16.16s} ".format(cc_name))
                for region in self.get_regions():
                    sys.stdout.write(" {:2s} {:s}".format(region, usage['per_region'][region].tostr()))
                sys.stdout.write("\n")
            else:
                sys.stdout.write("{:16.16s} ".format(cc_name))
                sys.stdout.write(" {:s}".format(usage['total'].tostr()))
                sys.stdout.write("\n")

        out_title()
        for cc_name in sorted(self.get_cost_centers().keys(),
                              reverse=True,
                              key=lambda cc_name: usage_per_cc[cc_name]['total'].ram):
            if not usage_per_cc[cc_name]['total'].nonzero():
                continue
            if insignificant_usage(usage_per_cc[cc_name]):
                small_usage['total'].add(usage_per_cc[cc_name]['total'])
                for region in self.get_regions():
                    small_usage['per_region'][region].add(usage_per_cc[cc_name]['per_region'][region])
                continue
            out_cc(cc_name, usage_per_cc[cc_name])
        if small_usage['total'].nonzero():
            out_cc('SMALL_CCs', small_usage)
        if total_usage['total'].nonzero():
            out_cc('TOTAL', total_usage)

class Flavor:

    def __init__(self, id, name, region,
                 ram, vcpus, disk):
        self.name   = name
        self.region = region
        self.ram    = ram
        self.vcpus  = vcpus
        self.disk   = disk

class UsagePerTenant:

    def __init__(self, id):
        self.id = id
        self.per_region = dict()

class UsagePerTenantRegion:

    def __init__(self, id, region):
        self.id         = id
        self.region     = region
        self.per_status = dict()

class UsagePerTenantRegionStatus:

    def __init__(self, id, region, status):
        self.id     = id
        self.region = region
        self.status = status
        self.usage  = ResourceUsage()

class ResourceUsage:

    def __init__(self, instances=0, ram=0, vcpus=0, disk=0):
        self.instances = instances
        self.ram       = ram
        self.vcpus     = vcpus
        self.disk      = disk

    def add(self, u2):
        self.instances += u2.instances
        self.ram       += u2.ram
        self.vcpus     += u2.vcpus
        self.disk      += u2.disk
        return self

    def nonzero(self):
        return self.instances > 0 \
            or self.ram       > 0 \
            or self.vcpus     > 0 \
            or self.disk      > 0

    def tostr(self):
        return "{:3d} VMs {:4d} vCPUs {:7d} MB {:7d} GB".\
            format(self.instances, self.vcpus, self.ram, self.disk)

class CostCenter:

    def __init__(
            self,
            name,
            tenants = []):
        self.name = name
        self.tenants = tenants[:]

ua = UsageAnalyzer(ram_cut = 0)

ua.collect_flavors()
ua.collect_servers()
ua.collect_volumes()
ua.tenant_report()
